@extends('layouts.frontend')
@section('title', $user->display_name.' | develop.ideas')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">
                        {{ $user->display_name }}
                    </h1>
                </div>
            </div>
        </div>
    </div>
@endsection