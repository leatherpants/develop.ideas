@extends('layouts.blank')
@section('title', 'develop.ideas')
@section('content')
    <div class="ui middle aligned center aligned grid">
        <div class="column">
            <h2 class="ui blue image header">
                <span class="content">
                    develop.ideas
                </span>
            </h2>
            <form action="{{ route('login') }}" method="post" class="ui large form">
                @csrf

                <div class="ui stacked segment">
                    <div class="{{ $errors->has('email') ? 'error ' : '' }}field">
                        <div class="ui left icon input">
                            <i class="user icon"></i>
                            <input type="text" name="email" id="email" placeholder="{{ trans('common.email') }}">
                        </div>
                    </div>
                    <div class="{{ $errors->has('password') ? 'error ' : '' }}field">
                        <div class="ui left icon input">
                            <i class="lock icon"></i>
                            <input type="password" name="password" placeholder="{{ trans('common.password') }}">
                        </div>
                    </div>
                    <button type="submit" class="ui fluid large blue submit button">{{ trans('common.login') }}</button>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('styles')
    <style type="text/css">
        body {
            background-color: #DADADA;
        }
        body > .grid {
            height: 100%;
        }
        .image {
            margin-top: -100px;
        }
        .column {
            max-width: 450px;
        }
    </style>
@endsection