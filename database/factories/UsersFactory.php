<?php

use Faker\Generator as Faker;

$factory->define(\App\developideas\Users\Models\Users::class, function (Faker $faker) {
    return [
        'email' => $faker->unique()->safeEmail,
        'password' => bcrypt('secret'),
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'display_name' => $faker->name,
        'user_name' => $faker->userName,
        'remember_token' => str_random(10),
    ];
});
