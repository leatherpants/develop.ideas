<?php

Route::group(['namespace' => 'Auth'], function () {
    Route::get('login', ['uses' => 'LoginController@getLogin', 'as' => 'login']);
    Route::post('login', ['uses' => 'LoginController@postLogin']);
    Route::get('logout', ['uses' => 'LoginController@logout', 'as' => 'logout']);
});

Route::group(['namespace' => 'Frontend'], function () {
    Route::get('', ['uses' => 'HomeController@index', 'as' => 'index']);

    Route::get('dashboard', ['uses' => 'HomeController@dashboard', 'as' => 'dashboard']);

    Route::get('{user_name}', ['uses' => 'UsersController@profile', 'as' => 'user']);
});
