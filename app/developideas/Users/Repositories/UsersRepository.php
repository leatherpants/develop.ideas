<?php

namespace App\developideas\Users\Repositories;

use App\developideas\Users\Models\Users;

class UsersRepository
{
    /**
     * @var Users
     */
    private $users;

    /**
     * UsersRepository constructor.
     * @param Users $users
     */
    public function __construct(Users $users)
    {
        $this->users = $users;
    }

    /**
     * Get data for user's profile by username.
     * @param string $user_name
     * @return mixed
     */
    public function getDataByUserName(string $user_name)
    {
        return $this->users
            ->where('user_name', $user_name)
            ->first();
    }
}
