<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\developideas\Users\Repositories\UsersRepository;

class UsersController extends Controller
{
    /**
     * @var UsersRepository
     */
    private $usersRepository;

    /**
     * UsersController constructor.
     * @param UsersRepository $usersRepository
     */
    public function __construct(UsersRepository $usersRepository)
    {
        $this->usersRepository = $usersRepository;
    }

    /**
     * Get user's profile by the username.
     * @param string $user_name
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function profile(string $user_name)
    {
        $user = $this->usersRepository->getDataByUserName($user_name);

        if (!$user) {
            abort(404);
        }

        return view('users.profile')
            ->with('user', $user);
    }
}
