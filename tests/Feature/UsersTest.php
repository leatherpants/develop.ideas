<?php

namespace Tests\Feature;

use Tests\TestCase;

class UsersTest extends TestCase
{
    /** @test */
    public function test_show_profile()
    {
        $response = $this->get(route('user', ['user_name' => $this->user->user_name]));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_show_profile_fails()
    {
        $response = $this->get(route('user', ['user_name' => 'xxx']));
        $response->assertNotFound();
    }
}
