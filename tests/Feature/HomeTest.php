<?php

namespace Tests\Feature;

use Tests\TestCase;

class HomeTest extends TestCase
{
    /** @test */
    public function test_show_the_landing_page()
    {
        $response = $this->get(route('index'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_show_dashboard_when_logged_in()
    {
        $response = $this->actingAs($this->user)->get(route('dashboard'));
        $response->assertSuccessful();
    }
}
