<?php

namespace Tests;

use App\developideas\Users\Models\Users;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, DatabaseMigrations;

    protected $user;

    /**
     * Set up the test.
     */
    public function setUp()
    {
        parent::setUp();

        $this->user = factory(Users::class)->create();
    }

    public function tearDown()
    {
        $this->artisan('migrate:reset');
        parent::tearDown();
    }
}
